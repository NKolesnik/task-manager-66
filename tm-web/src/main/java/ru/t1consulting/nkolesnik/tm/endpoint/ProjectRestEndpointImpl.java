package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.api.ProjectRestEndpoint;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;
import ru.t1consulting.nkolesnik.tm.service.ProjectDtoService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @Autowired
    ProjectDtoService projectService;

    @Override
    @PutMapping("/create")
    public void create() {
        projectService.create();
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsById(id);
    }

    @Override
    @GetMapping("/findById/{id}")
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<ProjectDto> findAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

    @Override
    @PostMapping("/save")
    public void save(@NotNull @RequestBody final ProjectDto project) {
        projectService.save(project);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final ProjectDto project) {
        projectService.delete(project);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        projectService.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<ProjectDto> projects) {
        projectService.deleteAll(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear();
    }

}
