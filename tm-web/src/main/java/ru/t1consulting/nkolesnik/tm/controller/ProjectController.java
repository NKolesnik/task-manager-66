package ru.t1consulting.nkolesnik.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;
import ru.t1consulting.nkolesnik.tm.service.ProjectDtoService;

import java.util.Collection;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @Nullable
    public Collection<ProjectDto> getProjects() {
        return projectService.findAll();
    }

    @NotNull
    public Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/project/create")
    public String create() {
        projectService.create();
        return "redirect:/projects";
    }

    @GetMapping("/projects")
    public ModelAndView list() {
        return new ModelAndView("projects", "projects", getProjects());
    }

    @NotNull
    @PostMapping("/project/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("project") final ProjectDto project,
            @NotNull final BindingResult result
    ) {
        projectService.save(project);
        return "redirect:/projects";
    }


    @NotNull
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String projectId) {
        @NotNull final ProjectDto project = projectService.findById(projectId);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        projectService.deleteById(id);
        return "redirect:/projects";
    }

}
