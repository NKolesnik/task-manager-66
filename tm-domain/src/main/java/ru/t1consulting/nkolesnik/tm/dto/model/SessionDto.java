package ru.t1consulting.nkolesnik.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.listener.EntityListener;

import javax.persistence.EntityListeners;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDto extends AbstractUserOwnedModelDto {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}
