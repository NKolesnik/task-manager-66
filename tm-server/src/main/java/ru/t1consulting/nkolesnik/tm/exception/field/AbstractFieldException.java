package ru.t1consulting.nkolesnik.tm.exception.field;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.exception.AbstractException;

public class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
    }

    public AbstractFieldException(@NotNull final String message) {
        super(message);
    }

    public AbstractFieldException(
            @NotNull final String message, @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractFieldException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}