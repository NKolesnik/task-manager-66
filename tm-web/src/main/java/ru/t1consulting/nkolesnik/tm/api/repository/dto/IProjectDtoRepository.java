package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

@Repository
public interface IProjectDtoRepository extends JpaRepository<ProjectDto, String> {

}
