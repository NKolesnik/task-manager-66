package ru.t1consulting.nkolesnik.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;
import ru.t1consulting.nkolesnik.tm.service.ProjectDtoService;
import ru.t1consulting.nkolesnik.tm.service.TaskDtoService;

import java.util.Collection;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private TaskDtoService taskService;

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @Nullable
    public Collection<TaskDto> getTaskDto() {
        return taskService.findAll();
    }

    @NotNull
    public Status[] getStatuses() {
        return Status.values();
    }

    @Nullable
    private Collection<ProjectDto> getProjects() {
        return projectService.findAll();
    }

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        taskService.create();
        return "redirect:/tasks";
    }

    @GetMapping("/tasks")
    public ModelAndView list() {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("tasks");
        modelAndView.addObject("tasks", getTaskDto());
        modelAndView.addObject("projectRepository", projectService);
        return modelAndView;
    }

    @NotNull
    @PostMapping("/task/edit/{id}")
    public String edit(@NotNull @ModelAttribute("task") final TaskDto task, @NotNull final BindingResult result) {
        taskService.save(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String taskId) {
        @NotNull final TaskDto task = taskService.findById(taskId);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", getStatuses());
        modelAndView.addObject("projects", getProjects());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        taskService.deleteById(id);
        return "redirect:/tasks";
    }

}
