package ru.t1consulting.nkolesnik.tm.endpoint;

import lombok.Getter;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Controller
@WebService
public final class CalculatorEndpoint {

    @WebMethod
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }
}
