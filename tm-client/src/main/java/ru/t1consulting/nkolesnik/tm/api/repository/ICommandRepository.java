package ru.t1consulting.nkolesnik.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandRepository {

    @Nullable
    Collection<AbstractListener> getTerminalCommands();

    void add(@NotNull AbstractListener command);

    @Nullable
    AbstractListener getCommandByName(@NotNull String name);

    @Nullable
    AbstractListener getCommandByArgument(@NotNull String argument);

    @NotNull
    Iterable<AbstractListener> getCommandsWithArguments();

}
