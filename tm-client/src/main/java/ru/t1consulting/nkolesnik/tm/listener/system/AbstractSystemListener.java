package ru.t1consulting.nkolesnik.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.endpoint.ISystemEndpoint;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.listener.AbstractListener;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    public ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    protected Collection<AbstractListener> getCommands() {
        return listeners;
    }

    protected Collection<AbstractListener> getArguments() {
        return listeners.stream()
                .filter(arg -> arg.getArgument() != null && !arg.getArgument().isEmpty())
                .collect(Collectors.toList());
    }


}
