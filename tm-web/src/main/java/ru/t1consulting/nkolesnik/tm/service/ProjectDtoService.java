package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    public void add(@NotNull final ProjectDto project) {
        projectRepository.saveAndFlush(project);
    }

    public void create() {
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName("New Project" + System.currentTimeMillis());
        projectRepository.saveAndFlush(project);
    }

    public boolean existsById(@NotNull final String id) {
        return projectRepository.existsById(id);
    }

    @Nullable
    public ProjectDto findById(@NotNull final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    public Collection<ProjectDto> findAll() {
        return projectRepository.findAll();
    }

    public long count() {
        return projectRepository.count();
    }

    public void save(@NotNull final ProjectDto project) {
        projectRepository.save(project);
    }

    public void deleteById(@NotNull final String id) {
        projectRepository.deleteById(id);
    }

    public void delete(@NotNull final ProjectDto project) {
        projectRepository.delete(project);
    }

    public void deleteAll(@NotNull final List<ProjectDto> projectList) {
        projectRepository.deleteAll(projectList);
    }

    public void clear() {
        projectRepository.deleteAll();
    }

}
