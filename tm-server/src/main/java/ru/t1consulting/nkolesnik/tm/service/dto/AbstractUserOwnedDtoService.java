package ru.t1consulting.nkolesnik.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDto;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDto>
        extends AbstractDtoService<M> implements IUserOwnedDtoService<M> {

}
