package ru.t1consulting.nkolesnik.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateRequest extends AbstractUserRequest {

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    public UserUpdateRequest(@Nullable String token) {
        super(token);
    }

    public UserUpdateRequest(
            @Nullable String token,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    ) {
        super(token);
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

}

