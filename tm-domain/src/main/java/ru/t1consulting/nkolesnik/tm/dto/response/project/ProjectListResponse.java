package ru.t1consulting.nkolesnik.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<ProjectDto> projects;

    public ProjectListResponse(@Nullable List<ProjectDto> projects) {
        this.projects = projects;
    }

}

