package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.SessionDto;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;

public interface IAuthService {

    @NotNull
    SessionDto validateToken(@Nullable String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    UserDto registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
