package ru.t1consulting.nkolesnik.tm.model.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ColumnDefault;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1consulting.nkolesnik.tm.api.model.IWBS;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class AbstractWbsModelDto extends AbstractModelDto implements IWBS {

    @NotNull
    protected static final String FORMAT = "yyyy-MM-dd";

    @NotNull
    @ColumnDefault("''")
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @ColumnDefault("''")
    @Column(nullable = false)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @ColumnDefault("'NOT_STARTED'")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = FORMAT)
    private Date created = new Date();

    @Nullable
    @Column(name = "date_begin")
    @DateTimeFormat(pattern = FORMAT)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @DateTimeFormat(pattern = FORMAT)
    private Date dateEnd;

    @Override
    public String toString() {
        return name + "; " + description + "; " + Status.toName(status);
    }

}
