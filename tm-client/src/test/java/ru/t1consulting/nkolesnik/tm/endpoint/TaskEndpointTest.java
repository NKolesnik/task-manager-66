package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.endpoint.*;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDto;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectCreateRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.task.*;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLoginRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLogoutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.project.ProjectCreateResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.task.*;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLoginResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserProfileResponse;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.marker.SoapCategory;

import java.util.List;
import java.util.UUID;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private static final String TASK_NAME = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final Status NEW_STATUS = Status.IN_PROGRESS;

    @NotNull
    private static final String NEW_TASK_NAME = "NEW_TEST_TASK_NAME";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "NEW_TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String TEST_USER_LOGIN = "test";

    @NotNull
    private static final String TEST_USER_PASSWORD = "test";

    @NotNull
    private static final String ADMIN_USER_LOGIN = "admin";

    @NotNull
    private static final String ADMIN_USER_PASSWORD = "admin";

    @NotNull
    private static final String BAD_TOKEN = UUID.randomUUID().toString();

    @Nullable
    private static final String NULL_TOKEN = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @NotNull
    private static final String BAD_TASK_ID = UUID.randomUUID().toString();

    @Nullable
    private static final Integer NULL_TASK_INDEX = null;

    @NotNull
    private static final Integer NEGATIVE_TASK_INDEX = -1;

    @NotNull
    private static final Integer BIG_TASK_INDEX = Integer.MAX_VALUE;

    @Nullable
    private static final String NULL_TASK_NAME = null;

    @Nullable
    private static final String NULL_TASK_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    private static final long COUNT_TEST_TASKS = 10L;

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();
    @NotNull
    private static final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance();
    @Nullable
    private static String adminToken;
    @Nullable
    private static String adminUserId;
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();
    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance();
    @Nullable
    private String testToken;
    @Nullable
    private String testUserId;
    @Nullable
    private String testProjectId;

    @Nullable
    private List<TaskDto> testTasksList;

    @Before
    public void setup() {
        @NotNull final UserLoginRequest adminLoginRequest = new UserLoginRequest(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        @NotNull final UserLoginResponse adminLoginResponse = authEndpoint.login(adminLoginRequest);
        adminToken = adminLoginResponse.getToken();
        @NotNull final UserProfileRequest adminProfileRequest = new UserProfileRequest(adminToken);
        @NotNull final UserProfileResponse adminProfileResponse = authEndpoint.profile(adminProfileRequest);
        @Nullable final UserDto adminUser = adminProfileResponse.getUser();
        adminUserId = adminUser.getId();
        @NotNull final UserLoginRequest testLoginRequest = new UserLoginRequest(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final UserLoginResponse testLoginResponse = authEndpoint.login(testLoginRequest);
        testToken = testLoginResponse.getToken();
        @NotNull final UserProfileRequest testProfileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse testProfileResponse = authEndpoint.profile(testProfileRequest);
        @Nullable final UserDto testUser = testProfileResponse.getUser();
        testUserId = testUser.getId();
        @NotNull final TaskListRequest listRequest = new TaskListRequest(testToken);
        createManyTestTasks();
        testProjectId = createTestProject();
        testTasksList = taskEndpoint.listTask(listRequest).getTasks();
        bindTasksToProject();
    }

    @After
    public void cleanup() {
        @NotNull final UserLogoutRequest testLogoutRequest = new UserLogoutRequest(testToken);
        @NotNull final TaskClearRequest clearRequest = new TaskClearRequest(testToken);
        taskEndpoint.clearTask(clearRequest);
        removeTestProject(testProjectId);
        authEndpoint.logout(testLogoutRequest);
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(testToken, NULL_TASK_NAME, NULL_TASK_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(testToken, TASK_NAME, NULL_TASK_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(BAD_TOKEN, TASK_NAME, TASK_DESCRIPTION))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(NULL_TOKEN, TASK_NAME, TASK_DESCRIPTION))
        );
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(testToken, TASK_NAME, TASK_DESCRIPTION);
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());
        Assert.assertEquals(TASK_NAME, createResponse.getTask().getName());
        Assert.assertEquals(testUserId, createResponse.getTask().getUserId());
        Assert.assertNotEquals(adminUserId, createResponse.getTask().getUserId());
        @NotNull final String testTaskId = createResponse.getTask().getId();
        removeTestTask(testTaskId);
    }

    @Test
    public void listTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest(BAD_TOKEN)));
        @NotNull final TaskListRequest listRequest = new TaskListRequest(testToken);
        @NotNull final TaskListResponse listResponse = taskEndpoint.listTask(listRequest);
        Assert.assertNotNull(listResponse);
        Assert.assertNotNull(listResponse.getTasks());
        Assert.assertEquals(COUNT_TEST_TASKS, listResponse.getTasks().size());
        @NotNull final TaskDto task = listResponse.getTasks().get(0);
        Assert.assertEquals(testUserId, task.getUserId());
        Assert.assertNotEquals(adminUserId, task.getUserId());
    }

    @Test
    public void getTaskById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(new TaskGetByIdRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(new TaskGetByIdRequest(testToken, NULL_TASK_ID))
        );
        Assert.assertNull(taskEndpoint.getTaskById(new TaskGetByIdRequest(testToken, BAD_TASK_ID)).getTask());
        Assert.assertNotNull(testTasksList);
        @NotNull final TaskDto task = testTasksList.get(0);
        @NotNull final String taskId = task.getId();
        @NotNull final TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(testToken, taskId);
        @NotNull final TaskGetByIdResponse getByIdResponse = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse);
        Assert.assertNotNull(getByIdResponse.getTask());
        Assert.assertEquals(testUserId, getByIdResponse.getTask().getUserId());
        Assert.assertEquals(TASK_NAME, getByIdResponse.getTask().getName());
        Assert.assertNotEquals(adminUserId, getByIdResponse.getTask().getUserId());
    }

    @Test
    public void getTaskByProjectId() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.getTaskByProjectId(new TaskGetByProjectIdRequest()));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByProjectId(new TaskGetByProjectIdRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskByProjectId(new TaskGetByProjectIdRequest(BAD_TOKEN))
        );
        @NotNull final TaskGetByProjectIdRequest getByProjectIdRequest =
                new TaskGetByProjectIdRequest(testToken, testProjectId);
        @NotNull final TaskGetByProjectIdResponse taskGetByProjectIdResponse =
                taskEndpoint.getTaskByProjectId(getByProjectIdRequest);
        Assert.assertNotNull(taskGetByProjectIdResponse);
        Assert.assertNotNull(taskGetByProjectIdResponse.getTasks());
        @NotNull final TaskDto task = taskGetByProjectIdResponse.getTasks().get(0);
        Assert.assertEquals(testProjectId, task.getProjectId());
        Assert.assertEquals(testUserId, task.getUserId());
        Assert.assertNotEquals(adminUserId, task.getUserId());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest()));
        Assert.assertNotNull(testTasksList);
        @NotNull final TaskDto task = testTasksList.get(0);
        @NotNull final String oldTaskName = task.getName();
        @NotNull final String oldTaskDescription = task.getDescription();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(testToken, NULL_TASK_ID, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(testToken, BAD_TASK_ID, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(testToken, task.getId(), NULL_TASK_NAME, NEW_TASK_DESCRIPTION)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(testToken, task.getId(), NEW_TASK_NAME, NULL_TASK_DESCRIPTION)
                )
        );
        @NotNull final TaskUpdateByIdRequest updateByIdRequest =
                new TaskUpdateByIdRequest(testToken, task.getId(), NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        taskEndpoint.updateTaskById(updateByIdRequest);
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(testToken, task.getId());
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNotNull(taskGetByIdResponse.getTask());
        @NotNull final TaskDto repositoryTask = taskGetByIdResponse.getTask();
        Assert.assertEquals(NEW_TASK_NAME, repositoryTask.getName());
        Assert.assertEquals(NEW_TASK_DESCRIPTION, repositoryTask.getDescription());
        Assert.assertNotEquals(oldTaskName, repositoryTask.getName());
        Assert.assertNotEquals(oldTaskDescription, repositoryTask.getDescription());
        Assert.assertEquals(testUserId, repositoryTask.getUserId());
        Assert.assertNotEquals(adminUserId, repositoryTask.getUserId());
    }

    @Test
    public void startTaskById() {
        Assert.assertNotNull(testTasksList);
        @NotNull final TaskDto task = testTasksList.get(0);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(new TaskStartByIdRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(testToken, NULL_TASK_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(testToken, BAD_TASK_ID))
        );
        @NotNull final TaskStartByIdRequest startByIdRequest = new TaskStartByIdRequest(testToken, task.getId());
        taskEndpoint.startTaskById(startByIdRequest);
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(testToken, task.getId());
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNotNull(taskGetByIdResponse.getTask());
        @NotNull final TaskDto repositoryTask = taskGetByIdResponse.getTask();
        Assert.assertEquals(Status.IN_PROGRESS, repositoryTask.getStatus());
        Assert.assertEquals(testUserId, repositoryTask.getUserId());
        Assert.assertNotEquals(adminUserId, repositoryTask.getUserId());
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertNotNull(testTasksList);
        @NotNull final TaskDto task = testTasksList.get(0);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(testToken, NULL_TASK_ID, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(testToken, BAD_TASK_ID, NEW_STATUS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(testToken, task.getId(), NULL_STATUS)
                )
        );
        @NotNull final TaskChangeStatusByIdRequest changeStatusByIdRequest =
                new TaskChangeStatusByIdRequest(testToken, task.getId(), NEW_STATUS);
        @NotNull final TaskChangeStatusByIdResponse changeTaskStatusByIdResponse =
                taskEndpoint.changeTaskStatusById(changeStatusByIdRequest);
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(testToken, task.getId());
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNotNull(taskGetByIdResponse.getTask());
        @NotNull final TaskDto repositoryTask = taskGetByIdResponse.getTask();
        Assert.assertEquals(Status.IN_PROGRESS, repositoryTask.getStatus());
        Assert.assertEquals(testUserId, repositoryTask.getUserId());
        Assert.assertNotEquals(adminUserId, repositoryTask.getUserId());
    }

    @Test
    public void completeTaskStatusById() {
        Assert.assertNotNull(testTasksList);
        @NotNull final TaskDto task = testTasksList.get(0);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest()));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(testToken, NULL_TASK_ID))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(testToken, BAD_TASK_ID))
        );
        @NotNull final TaskCompleteByIdRequest completeByIdRequest =
                new TaskCompleteByIdRequest(testToken, task.getId());
        taskEndpoint.completeTaskStatusById(completeByIdRequest);
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(testToken, task.getId());
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNotNull(taskGetByIdResponse.getTask());
        @NotNull final TaskDto repositoryTask = taskGetByIdResponse.getTask();
        Assert.assertEquals(Status.COMPLETED, repositoryTask.getStatus());
        Assert.assertEquals(testUserId, repositoryTask.getUserId());
        Assert.assertNotEquals(adminUserId, repositoryTask.getUserId());
    }

    @Test
    public void removeTaskById() {
        Assert.assertNotNull(testTasksList);
        @NotNull final TaskDto task = testTasksList.get(0);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(testToken)));
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(testToken, NULL_TASK_ID))
        );
        @NotNull final TaskRemoveByIdRequest removeByIdRequest = new TaskRemoveByIdRequest(testToken, task.getId());
        taskEndpoint.removeTaskById(removeByIdRequest);
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(testToken, task.getId());
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNull(taskGetByIdResponse.getTask());
    }


    private void createManyTestTasks() {
        for (int i = 0; i < COUNT_TEST_TASKS; i++) {
            @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(testToken);
            createRequest.setName(TASK_NAME);
            createRequest.setDescription(TASK_DESCRIPTION);
            taskEndpoint.createTask(createRequest);
        }
    }

    private void bindTasksToProject() {
        if (testTasksList != null) {
            for (TaskDto task : testTasksList) {
                @NotNull final TaskBindToProjectRequest bindToProjectRequest = new TaskBindToProjectRequest(testToken);
                bindToProjectRequest.setProjectId(testProjectId);
                bindToProjectRequest.setTaskId(task.getId());
                projectTaskEndpoint.bindTaskToProject(bindToProjectRequest);
            }
        }
    }

    private void removeTestTask(@NotNull final String testTaskId) {
        @NotNull final TaskRemoveByIdRequest removeByIdRequest = new TaskRemoveByIdRequest(testToken);
        removeByIdRequest.setId(testTaskId);
        taskEndpoint.removeTaskById(removeByIdRequest);
    }

    @NotNull
    private String createTestProject() {
        if (testProjectId == null) {
            @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
            projectCreateRequest.setName(PROJECT_NAME);
            projectCreateRequest.setDescription(PROJECT_DESCRIPTION);
            @NotNull final ProjectCreateResponse projectCreateResponse = projectEndpoint.
                    createProject(projectCreateRequest);
            return projectCreateResponse.getProject().getId();
        } else {
            return testProjectId;
        }
    }

    private void removeTestProject(@NotNull final String testProjectId) {
        @NotNull final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(testToken);
        removeByIdRequest.setId(testProjectId);
        projectEndpoint.removeProjectById(removeByIdRequest);
    }
}
