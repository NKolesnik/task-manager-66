package ru.t1consulting.nkolesnik.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserUnlockRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserUnlockResponse;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public class UserUnlockListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-unlock";

    @NotNull
    public static final String DESCRIPTION = "Unlock user in system...";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userUnlockListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[USER UNLOCK]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        @NotNull final UserUnlockResponse response = getUserEndpoint().unlockUser(request);
        if (!response.getSuccess()) throw new UserNotFoundException();
        System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
