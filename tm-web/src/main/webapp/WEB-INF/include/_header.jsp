<html>
    <head>
        <meta charset="utf-8">
        <title>Task Manager</title>
        <style>
            h1 {
                font-size: 1.6em;
            }
            a {
                color: darkblue;
            }
            select {
                width: 200px;
            }
            input[type="text"] {
                width: 200px;
            }
            input[type="date"] {
                width: 200px;
            }
            td {
                padding: 10px;
                border: solid 1px;
            }
        </style>
    </head>
    <body>
        <table width="100%" height="100%" style="border-collapse: collapse;">
            <tr>
                <td height="35" width="200" nowrap="nowrap" align="center">
                    <a href="/"><b>Task Manager</b></a>
                </td>
                <td width="100%" align="right">
                    <a href="/projects">PROJECTS</a>
                    <a href="/tasks">TASKS</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" height="100%" vailgn="top">