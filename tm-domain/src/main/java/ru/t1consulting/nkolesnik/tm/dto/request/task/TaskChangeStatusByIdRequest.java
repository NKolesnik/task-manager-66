package ru.t1consulting.nkolesnik.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable String token) {
        super(token);
    }

    public TaskChangeStatusByIdRequest(@Nullable String token, @Nullable String id, @Nullable Status status) {
        super(token);
        this.id = id;
        this.status = status;
    }

}
