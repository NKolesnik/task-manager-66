package ru.t1consulting.nkolesnik.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataBinarySaveRequest extends AbstractUserRequest {

    public DataBinarySaveRequest(@Nullable String token) {
        super(token);
    }

}
