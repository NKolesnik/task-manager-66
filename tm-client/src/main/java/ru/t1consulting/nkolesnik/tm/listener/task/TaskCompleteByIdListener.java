package ru.t1consulting.nkolesnik.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDto;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskCompleteByIdResponse;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setId(id);
        @NotNull final TaskCompleteByIdResponse response = getTaskEndpoint().completeTaskStatusById(request);
        @Nullable TaskDto task = response.getTask();
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
