package ru.t1consulting.nkolesnik.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.model.ICommand;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ArgumentsListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String DESCRIPTION = "Show arguments list.";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@argumentsListener.getName() == #event.name || " +
            "@argumentsListener.getArgument() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractListener> commands = getArguments();
        for (ICommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) {
                continue;
            }
            System.out.println(argument);
        }
    }

}
