package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.api.TaskRestEndpoint;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;
import ru.t1consulting.nkolesnik.tm.service.TaskDtoService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    @Autowired
    TaskDtoService taskService;

    @Override
    @PutMapping("/create")
    public void create() {
        taskService.create();
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskService.existsById(id);
    }

    @Override
    @GetMapping("/findById/{id}")
    public TaskDto findById(@NotNull @PathVariable("id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<TaskDto> findAll() {
        return taskService.findAll();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

    @Override
    @PostMapping("/save")
    public void save(@NotNull @RequestBody final TaskDto task) {
        taskService.save(task);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final TaskDto task) {
        taskService.delete(task);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        taskService.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<TaskDto> tasks) {
        taskService.deleteAll(tasks);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear();
    }

}
