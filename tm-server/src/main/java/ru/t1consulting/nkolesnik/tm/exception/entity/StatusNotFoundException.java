package ru.t1consulting.nkolesnik.tm.exception.entity;

public final class StatusNotFoundException extends AbstractEntityNotFoundException {

    public StatusNotFoundException() {
        super("Error! Status not found...");
    }

}