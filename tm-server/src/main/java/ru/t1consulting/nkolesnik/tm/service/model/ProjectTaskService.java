package ru.t1consulting.nkolesnik.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectTaskService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.model.ITaskService;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.List;
import java.util.Optional;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsById(projectId)) return;
        @NotNull final Task task = Optional.ofNullable(taskService.findById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        @NotNull final Project project = Optional.ofNullable(projectService.findById(userId, projectId)).
                orElseThrow(ProjectNotFoundException::new);
        task.setProject(project);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskService.findById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProject(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        tasks.forEach(task -> taskService.removeById(userId, task.getId()));
        projectService.removeById(projectId);
    }

}
