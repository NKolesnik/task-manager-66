package ru.t1consulting.nkolesnik.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import java.util.Collection;
import java.util.List;

@RequestMapping("/api/projects")
public interface ProjectRestEndpoint {

    @PutMapping("/create")
    void create();

    @GetMapping("/existById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/findById/{id}")
    ProjectDto findById(@PathVariable("id") String id);

    @GetMapping("/findAll")
    Collection<ProjectDto> findAll();

    @GetMapping("/count")
    long count();

    @PostMapping("/save")
    void save(@RequestBody ProjectDto project);

    @PostMapping("/delete")
    void delete(@RequestBody ProjectDto project);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<ProjectDto> project);

    @DeleteMapping("/clear")
    void clear();

}
